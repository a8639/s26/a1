console.log("Welcome to activity");


const HTTP = require('http');


HTTP.createServer((req, res) => {
    
    if(req.url ==="/register"){
        res.writeHead(400, {"Content-Type": "text/plain"});
        res.write("I'm sorry the page you are looking for cannot be found");
        res.end()
    } else if(req.url ==="/login") {
        res.writeHead(200, {"Content-Type": "text/plain"});
        res.write("Welcome to the login page");
        res.end();
    } else {
        res.writeHead(200, {"Content-Type": "text/plain"});
        res.write("This is the response from the server");
        res.end();
    }

}).listen(3000);

//What directive is used by Node.js in loading the modules it needs?
//answer: require method

//What Node.js module contains a method for server creation?
//answer: http module

//What is the method of the http object responsible for creating a server using Node.js?
//answer: creteServer()

//What method of the response object allows us to set status codes and content types?
//answer: writeHead()

//Where will console.log() output its contents when run in Node.js?
//answer: terminal

//What property of the request object contains the address' endpoint?
//answer: incomingMessage.url

